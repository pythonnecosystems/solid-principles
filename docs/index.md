# OOP의 SOLID 원칙 <sup>[1](#footnote_1)</sup>

SOLID 원칙은 객체 지향 프로그래밍(OOP)이 바르게 수행될 수 있도록 하는 기본 개념 집합이다. SOLID는 [Robert C. Martin](https://en.wikipedia.org/wiki/Robert_C._Martin)이 처음으로 관련 원칙의 집합으로 소개하였으며, 이후 소프트웨어 엔지니어링 분야의 표준이 되었다. 이 문서에서는 C#의 몇 가지 간단한 예를 통해 SOLID의 다섯 가지 원칙을 실용적인 측면에서 설명한다.

<a name="footnote_1">1</a>: 이 페이지는 [SOLID Principles of OOP](https://medium.com/swlh/solid-principles-of-oop-c24bd6ccde77)를 편역한 것임.

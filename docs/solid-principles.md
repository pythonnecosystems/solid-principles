OOP의 SOLID 원칙은 단일 책임 원칙(Single Responsibility Principle), 개방형-폐쇄형 원칙(Open-Closed Principle), Liskov Substitution 원칙(LSP), 인터페이스 분리 원칙(ISP, Interface Segregation Principle) 및 의존성 반전 원칙(DIP, Dependency Inversion Principle)이다. SOLID 원칙은 OOP 어플리케이션의 가독성, 테스트 가능성, 확장성 및 유지보수 가능성을 보장한다.

## 단일 책임 원칙(Single Responsibility Principle) 
**SRP**는 각 컴포넌트가 하나의 책임만 가져야 한다고 규정한다. 이렇게 하면 클래스와 그 메서드를 쉽게 읽고 이해할 수 있으며, 변경 사항이 변경이 필요한 컴포넌트에만 영향을 미치도록 할 수 있다. 다음 코드 스니펫에는 SRP 원칙을 위반하는 예가 나타나 있다. 이 경우 클래스에는 세금 계산하는 작업이 있다. 세금 규정은 주마다 다르므로 이 클래스는 여러 주에 대한 세금을 계산할 수 있어야 한다.

```C#
class TaxCalculator
{
   private string Province { get; set; }
   private double TaxPercentage { get; set; }
   public TaxCalculator(string province)
     {
        Province = province;

        switch(province)
        {
          case "Quebec":
            Province = "Quebec";
            TaxPercentage = 14.975;
            break;
          default:
            Province = "BritishColumbia";
            TaxPercentage = 12;
            break;
        }
            
        }
        public double calculate_tax(Product product)
        {
          double result = 0;
          double result = Math.Round(product.Price*TaxPercentage/100,2);
          return result;
        }

}
public class Product
{
   public string Name { get; }
   public double Price { get; }
   public string Category { get; }
   public bool Imported { get; }
   public Product(string name, double price, string category, bool         imported)
   {
      Name = name;
      Price = price;
      Category = category;
      Imported = imported;
   }
}
```

이 솔루션은 효과가 있지만 SRP 원칙을 위반하면 테스트와 유지 보수가 어렵다. 예를 들어, 한 주에서 세율을 변경하기로 결정하면 전체 클래스가 변경되어야 하므로 다른 주에 의도하지 않은 부작용이 발생할 수 있다.

솔루션은 코드에 추상화를 도입하고 주별 세금 계산기 클래스 구현을 분리하여 SRP 위반을 해결한다.

```C#
public abstract class TaxCalculator
{
   public abstract double TaxPercentage { get;  }
   public abstract double calculate_sales_tax(Product product);
}

public class BritishColumbiaTaxCalculator : TaxCalculator
{
  override public double TaxPercentage { get; } = 12;

  override public double calculate_sales_tax(Product product)
  {
    return Math.Round(product.Price*TaxPercentage/100,2);
  }
}
public class QuebecTaxCalculator : TaxCalculator
  {
    override public double TaxPercentage { get; } = 14.975;
    override public double calculate_sales_tax(Product product)
    {
      return Math.Round(product.Price * TaxPercentage/100,2);
    }
}
```

이제 한 주를 변경해도 다른 주에는 영향을 미치지 않는다. 또한 세금 계산기 구현에 대한 단위 테스트의 리팩토링은 변경된 구현에 대해서만 수행하면 된다.

## 개방형-폐쇄형 원칙(Open-Closed Principle) 
**OCP**는 OOP 엔티티가 확장을 위해서는 개방적이어야 하지만 수정을 위해서는 폐쇄적이어야 한다고 규정하고 있다. 이 원칙의 목적은 기존 컴포넌트가 변화하는 요구사항에 탄력적으로 대응할 수 있도록 하는 것이다. OCP를 구현하기 위해 추상화, 인터페이스, 제네릭을 비롯한 여러 가지 개념을 OOP 언어 내에서 사용할 수 있다. 이러한 개념을 사용하면 기본과 세부 사항을 분리할 수 있다. 확장을 위한 개방성은 특정 기능을 확장하기 위해 새로운 구현을 추가할 수 있음을 의미하며, 수정을 위한 폐쇄성은 구성 요소의 기본 기능이 변경되지 않음을 의미한다.

위의 세금 계산기에서 개선된 예는 추상화를 사용하고 해당 추상화에서 상속을 통해 확장을 허용함으로써 이미 부분적으로 OCP를 충족한다. 새로운 주가 추가되면 추상화를 수정하거나 기존의 특정 구현을 수정할 필요 없이 새로운 하위 `TaxCalculator` 클래스를 생성하면 된다.

이제 Quebec에서는 모든 수입품에 대해 5%의 세금을 추가로 적용하기로 결정했다면 어떻게 하여야 할까? 다음 솔루션은 변경 사항을 구현하지만 수입세에 대한 추가 메서드를 추가하도록 하여 클래스가 변경되었기 때문에 OCP를 위반하는 것이다.

```C#
public class QuebecTaxCalculator : TaxCalculator
{
  override public double TaxPercentage { get; } = 14.975;
  override public double calculate_sales_tax(Product product){
    return Math.Round(product.Price*(TaxPercentage)/100,2);
  }
  public double calculate_import_tax(Product product)
  {
    return Math.Round(product.Price*5/100,2);
  }
}
```

이 변경 사항을 구현하는 방법은 다음과 같이 상속을 사용하여 수입품에 대한 특정 구현을 만드는 것이다.

```C#
public class ImportQuebecTaxCalculator : QuebecTaxCalculator
{
  public double calculate_import_tax(Product product)
  {
    return Math.Round(product.Price*5/100,2);
  }  
}
```

이제 더 많은 기능을 추가하여 코드의 기능을 확장하는 동시에(확장용 개방형) 기존 코드를 변경하지 않고(수정용 폐쇄형) 코드의 기능을 확장할 수 있게 되었다.

OCP의 중요한 이점은 기존 단위 테스트는 리팩토링할 필요 없이 새로운 특정 구현을 다루기 위한 추가 테스트로만 확장할 수 있다는 것이다.

## Liskov Substitution 원칙(LSP) 

> "오리처럼 생겼고 오리처럼 꽥꽥거리는데 배터리가 필요하다면 추상화가 잘못되었을 가능성이 높다."

**LSP**는 자식 클래스의 객체가 베이스 클래스의 객체를 대체할 경우 OOP 어플리케이션이 바르게 동작한다는 것을 의미한다. 아주 간단하게 설명하자면, 사과 클래스의 객체가 어플리케이션을 중단시키지 않고 과일 클래스의 객체를 대체할 수 있어야 한다는 뜻이다.

LSP는 두 가지를 확인함으로써 달성될 수 있다. 첫째, 부모 클래스의 메서드를 재정의하는 자식 클래스의 모든 메서드는 입력과 동일한 수의 인수를 받아들여야 하며, 인수의 타입에 있어서는 부모 클래스만큼 제한적이거나 그보다 작아야 한다. 둘째, 재정의된 메서드의 반환 값은 부모 클래스 메서드의 반환 값과 동일한 타입이어야 하며 제한적이거나  작아야 한다.

세금 계산기로 돌아가면 다음 예제는 두 가지 측면에서 모두 LSP를 위반하고 있다.

```C#
public abstract class TaxCalculator<T>
{
   public abstract double TaxPercentage { get;  }
   public T calculate_sales_tax(IProduct product);
}

public class BritishColumbiaTaxCalculator : TaxCalculator
{
  override public double TaxPercentage { get; } = 12;

  override public double calculate_sales_tax(Product product)
  { 
    double tax;
    if (product.Imported)
    {
      tax = Math.Round(calculate_tax(product)+product.Price*5/100);
    } else
    {
      tax = Math.Round(calculate_tax(product));
    }
    
    return tax;
  }
}
public interface IProduct
{
  public string Name { get; }
  public double Price { get; }
  public string Category { get; }
}
```

`TaxCalculator` 추상화가 변경된 것을 주목하자. 첫째, 이제 `calculate_sales_tax` 메서드에 반환 타입을 부과하지 않는 제네릭이 되었다. 둘째, 메서드의 인수 타입으로 인터페이스를 사용했다. 반면에 `BritishColumbiaTaxCalculator` 서브클래스는 메서드를 재정의하고 인자와 반환 값 모두에 더 엄격한 타입을 부과한다. 눈치챘겠지만, 자식 클래스는 `Product` 인터페이스 대신 `Product` 클래스를 사용할 뿐만 아니라 `If` 문에서 `Product.Imported` 속성을 사용한다. 이것이 바로 인용문에서 오리에 대한 언급한 것과 같은 것이다.

참고로, C#과 같이 정적으로 타입이 지정된 좋은 언어는 이 예제에 표시된 위반 유형에 대해 컴파일되지 않는다. 이것이 바로 이 원칙이 얼마나 기본적이고 소프트웨어 개발 환경에 내재되어 있는지를 보여준다. 반면에 Python과 같이 느슨하게 타입이 지정된 언어는 (자신의 책임과 위험에 따라) 위반을 피할 수 있다.

## 인터페이스 분리 원칙(ISP, Interface Segregation Principle) 
**ISP**는 클래스가 필요하지 않은 멤버를 구현할 필요가 없다고 규정한다. 인터페이스를 지원하는 언어에서는 이 원칙을 문자 그대로 받아들여 모든 사용 사례를 캡처하기 위해 인터페이스를 설계하지 말자. 대신 각 사용 사례의 최소 요구 사항(일명 구현)에 맞게 서로 다른 인터페이스를 만들어야 한다. ISP를 준수하지 않으면 통합 인터페이스를 통해 클래스를 결합하는 기존 클래스의 불필요한 리팩터링과 같은 명백한 설계 결함이 발생하고 코드를 이해하고 유지 관리하기가 더 어려워진다.

세금 계산기에 대한 예를 계속 진행할 수도 있지만, 좀 더 재미있게 해보겠다. 다음 예는 단일 인터페이스에서 여러 유형의 차량에 대한 메서드 서명을 집계하여 ISP 원칙을 위반하고 있다.

```C#
public interface ITransportVehicle
{
  public void Move(double speed);
  public void Fly(double altitude);
}
public class Airplane: ITransportVehicle
{
  public void Move(double speed)
  {
    Console.WriteLine("Movind at speed "+speed+" mph");
   }
  public void Fly(double altitude)
  {
    Console.WriteLine("Flying at altitude "+altitude+" feet");
  }
}
public class Truck : ITransportVehicle
{
  public void Move(double speed)
  {
    Console.WriteLine("Movind at speed " + speed + " mph");
  }
  public void Fly(double altitude)
  {
    Console.WriteLine("Are you high?");
  }
}
```

분명히 트럭은 날 수 없지만 `TransportVehicle` 인터페이스는 지상 차량에 대해서도 `Fly` 메서드 구현을 무의미하게 강요한다. 이 문제를 해결하는 한 가지 방법은 다음과 같이 인터페이스를 지상 차량용 인터페이스와 항공기용 인터페이스로 분리하는 것이다.

```C#
public interface IGroundVehicle
{
  public void Move(double speed);
}
public interface IAircraft
{
  public void Move(double speed);
  public void Fly(double altitude);
}
public class Airplane: IAircraft 
{
  public void Move(double speed)
  {
    Console.WriteLine("Movind at speed "+speed+" mph");
  }
  public void Fly(double altitude)
  {
    Console.WriteLine("Flying at altitude "+altitude+" feet");
  }
}
public class Truck : IGroundVehicle
{
  public void Move(double speed)
  {
    Console.WriteLine("Movind at speed " + speed + " mph");
  }
}
```

Good! 이제 적어도 fly 메서드 시그니처가 변경되어도 `Truck` 클래스를 리팩터링할 필요가 없다.

인터페이스 개념을 지원하지 않는 언어의 경우 베이스 클래스와 추상화로 ISP를 적용할 수 있다.

## 의존성 반전 원칙(DIP, Dependency Inversion Principle)

> "A) 상위 레벨 모듈은 하위 레벨 모듈에 의존해서는 안 됩니다. 둘 다 추상화(예: 인터페이스)에 의존해야 합니다.
>
> B) 추상화는 세부 사항에 의존해서는 안 된다. 세부 사항(구체적인 구현)은 추상화에 의존해야 합니다."

**DIP**는 하이 레벨(기본 로직) 컴포넌트가 로우 레벨(특정 로직) 컴포넌트에 의존해서는 안 된다고 규정하고 있다.

하이 레벨과 로우 레벨 클래스 모두에 의존하는 추상화를 도입하여 코드의 로우 레벨과 하이 레벨 컴포넌트 간의 분리를 달성함으로써 DIP를 달성할 수 있다. DIP는 로우 레벨 컴포넌트와 하이 레벨 컴포넌트 간의 종속성 방향을 뒤집는 것이 아니라 공통 추상화를 통해 이들을 분리하는 것이다.

다양한 종류의 자동차를 예약할 수 있어야 하는 렌터카 회사 예약 어플리케이션을 예로 들어 보자. 어플리케이션을 다음과 같이 설계하면 DIP를 위반하는 것이다.

```C#
public class ReservationApp
{
  public List<CompactCar> CompactCars {get; set;}
  public List<LuxuryCar> LuxuryCars { get; set; }  
  public ReservationApp(CompactCars cars, LuxuryCars cars)
  {
    // Adds some cars...
  }
  public bool reserveCompact(CompactCar car, Client client)
  {
    if (!CompactCars.Contains(car) && car.Client != null)
    {
      car.Reserve(client);
      return true;
    }else 
    {
      Console.WriteLine("The car was not found or is reserved");
      return false;
    }
  }
  public bool reserveLux(LuxuryCar car, Client client)
  {
    if (!LuxuryCars.Contains(car) && car.Client != null)
    {
      car.Reserve(client);
      return true;
    } else
    {
      Console.WriteLine("The car was not found or is reserved");
      return false;
    }
  }
}
public class CompactCar
{
  public string ID { get; }
  // ... make, model, year, mileage, etc.
  public Client Client { get; set; }
  public void Reserve(Client client)
  {
    Client = client;
  }
  public bool hasGPS()
  {
    // ... method checks and returns true/false
  }
}
public class LuxuryCar
{
  public string ID { get; }
  // ... make, model, year, mileage, etc.
  public Client Client { get; set; }
  public void Reserve(Client client)
  {
    Client = client;
  }
  public bool hasSatelliteRoadsideAssistance()
  {
    // ... method checks and returns true/false
  }
}
public class Client
{
  public string Name { get; }
  public string Phone { get; set; }
  public Client(string name, string phone)
  {
    Name = name;
    Phone = phone;
  }
}
```

이제 알겠다. 지금까지 주의를 기울였다면 이 예에서 하나 이상의 SOLID 위반을 발견할 수 있다. 잠시 후에 다시 다룰 DIP를 위반하는 것외에도 `ReservationApp` 클래스는 OCP를 위반했다. 새로운 차량 카테고리를 추가하면 반드시 해당 클래스를 리팩토링해야 하므로 `ReservationApp` 클래스는 OCP가 아니다.

이제 DIP로 돌아가 보자. reservation app 예에서 `reservationApp` 클래스는 하이 레벨 모듈이고 자동차는 로우 레벨 모듈이다. DIP에서 이해하기 조금 어려울 수 있는 한 가지 측면은 컴포넌트가 하이 레벨에서 로우 레벨으로 어떻게 순위가 매겨지는가 하는 것이다. 이를 위해서는 앱의 주요 비즈니스 용도가 무엇인가라는 도메인 관점에서 접근해야 한다. 기본적으로 자동차 예약 앱의 주요 용도는 자동차를 예약하는 것이다. 따라서 예약을 관리하는 모듈은 논리적으로 하이 레벨(보다 근본적인) 컴포넌트이고, 예약 대상이 되는 구체적인 것은 자동차이다. 이전 코드 예의 문제점은 특정 자동차 유형에 따라 상위 컴포넌트가 달라진다는 것이다. 특정 요구 사항이 변경되어 새로운 카테고리의 자동차가 추가될 경우 현재 모듈을 리팩토링해야 하므로 확장 가능하고 유지 관리가 쉬운 설계가 아니다. DIP를 충족하기 위해 다음과 같이 리팩토링하여 하이 레벨 컴포넌트와 로우 레벨 컴포넌트가 모두 의존하는 인터페이스를 도입할 수 있다.

```C#
public class ReservationApp
{
  public List<IVehicle> Cars {get; set;}
  public ReservationApp(List<IVehicle> cars)
  {
    // Adds some cars...
  }
  public bool reserveVehicle(IVehicle car, Client client)
  {
    if (!Cars.Contains(car) && car.Client != null)
    {
      car.Reserve(client);
      return true;
    } else
    {
      Console.WriteLine("The car was not found or is reserved");
      return false;
    }
  }
}
public interface IVehicle
{
  public string ID { get; }
  public Client Client { get; set; }
  public void Reserve(Client client);
}
public class CompactCar: IVehicle
{
  public string ID { get; }
  public Client Client { get; set; }
  public void Reserve(Client client)
  {
    Client = client;
  }
}
public class LuxuryCar : IVehicle
{
  public string ID { get; }
  public Client Client { get; set; }
  public void Reserve(Client client)
  {
    Client = client;
  }
}
```

이것으로 DIP는 만족한다. `ReservationApp` 클래스에서 예외를 발생시킬 위험 없이 `IVehicle` 인터페이스를 구현하는 보다 구체적인 car 클래스로 대체할 수 있기 때문에 OCP 문제도 해결되고 LSP를 준수한다. 이 마지막 관찰은 DIP에 대한 흥미로운 사실이다. DIP는 OCP와 LSP 준수와 밀접한 관련이 있다.

## 최종 노트
세금 계산기 예에서 SRP 위반을 해결함으로써 우연찮게 OCP 원칙을 충족하게 된 것을 보았나요? 추상화인 베이스 클래스에 대한 각 주별 구현을 분리함으로써 베이스는 확장에 개방적이지만 수정에는 폐쇄적으로 만들었다(적어도 요구 사항 변경에 새로운 주를 추가해야 하는 경우). 세금 계산기에 관한 한 OCP는 이제 끝났다는 뜻인가? 전혀 아니다.

SOLID의 흥미로운 측면 중 하나이자 자주 언급되지는 않지만 한 가지 흥미로운 점은 SOLID를 적용하기 위한 프레임워크, 사고방식, 가이드라인 등 정해진 방법이 없다는 것이다. 따라서 개발자가 원칙을 얼마나 능숙하게 해석하고 적용하느냐에 따라 어플리케이션은 어느 정도 "견고(solid)"할 수 있다. 마감일을 지키면서 더 "견고한" 소프트웨어를 개발하기 위해 노력할 수는 있지만, 절대적인 SOLID를 달성하려고 하면 궁극적으로 프로젝트가 탈선하고 불필요한 복잡성이 추가될 수 있다. 설계와 개발 과정에서 경험과 사려 깊음이 결합된 도메인 지식은 특정 사용 사례를 목표로 삼고 향후 요구 사항이 어떻게 변화할지 예측하는 것이 SOLID 원칙의 적용 범위를 넓힐 수 있는 유일한 방법이다.
